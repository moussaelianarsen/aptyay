/*
   Aptyay: Changes yay's syntax to be more like APT
   Copyright (C) 2020 Arsen Musayelyan

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
)

func printHelpScreen()  {
	fmt.Println("Changes yay's syntax to be more like APT")
	fmt.Println("Usage: aptyay <Command> [Packages]")
	fmt.Println("")
	fmt.Println("Commands:")
	fmt.Println("help: Show this help screen")
	fmt.Println("list: List installed packages")
	fmt.Println("install: Install a package")
	fmt.Println("remove: Remove a package")
	fmt.Println("autoremove: Remove all unused dependencies")
	fmt.Println("search: Search for a package")
	fmt.Println("filesearch: Search for a file contained in a package")
	fmt.Println("update: Update repositories")
	fmt.Println("upgrade: Upgrade all packages")
	fmt.Println("interactive: Search in interactive mode")
}

func main()  {
	args := os.Args[1:]
	if len(args) == 0 {
		printHelpScreen()
		os.Exit(0)
	} else if args[0] == "help" || args[0] == "--help" {
		printHelpScreen()
		os.Exit(0)
	} else if args[0] == "install" {
		cmdArr :=[]string{"yay", "-S", strings.Join(args[1:], " ")}
		cmdStr := strings.Join(cmdArr, " ")
		yayCommand := exec.Command("sh", "-c", cmdStr)
		yayCommand.Stdout = os.Stdout
		yayCommand.Stdin = os.Stdin
		yayCommand.Stderr = os.Stderr
		yayError := yayCommand.Run()
		if yayError != nil {
			fmt.Println("Error thrown by yay")
			log.Fatal(yayError)
		}
	} else if args[0] == "remove" {
		cmdArr :=[]string{"yay", "-R", strings.Join(args[1:], " ")}
		cmdStr := strings.Join(cmdArr, " ")
		yayCommand := exec.Command("sh", "-c", cmdStr)
		yayCommand.Stdout = os.Stdout
		yayCommand.Stdin = os.Stdin
		yayCommand.Stderr = os.Stderr
		yayError := yayCommand.Run()
		if yayError != nil {
			fmt.Println("Error thrown by yay")
			log.Fatal(yayError)
		}
	} else if args[0] == "autoremove" {
		yayCommand := exec.Command( "sh", "-c", "yay -Rss $(yay -Qdtq)")
		yayCommand.Stdout = os.Stdout
		yayCommand.Stdin = os.Stdin
		yayCommand.Stderr = os.Stderr
		yayError := yayCommand.Run()
		if yayError != nil {
			fmt.Println("Error thrown by yay")
			log.Fatal(yayError)
		}
	} else if args[0] == "search" {
		cmdArr :=[]string{"yay", "-Ss", strings.Join(args[1:], " ")}
		cmdStr := strings.Join(cmdArr, " ")
		yayCommand := exec.Command("sh", "-c", cmdStr)
		yayCommand.Stdout = os.Stdout
		yayCommand.Stdin = os.Stdin
		yayCommand.Stderr = os.Stderr
		yayError := yayCommand.Run()
		if yayError != nil {
			fmt.Println("Error thrown by yay")
			log.Fatal(yayError)
		}
	} else if args[0] == "filesearch" {
		cmdArr :=[]string{"yay", "-Fx", strings.Join(args[1:], " ")}
		cmdStr := strings.Join(cmdArr, " ")
		yayCommand := exec.Command("sh", "-c", cmdStr)
		yayCommand.Stdout = os.Stdout
		yayCommand.Stdin = os.Stdin
		yayCommand.Stderr = os.Stderr
		yayError := yayCommand.Run()
		if yayError != nil {
			fmt.Println("Error thrown by yay")
			log.Fatal(yayError)
		}
	} else if args[0] == "refresh" {
		yayCommand := exec.Command( "sh", "-c", "yay -Syy")
		yayCommand.Stdout = os.Stdout
		yayCommand.Stdin = os.Stdin
		yayCommand.Stderr = os.Stderr
		yayError := yayCommand.Run()
		if yayError != nil {
			fmt.Println("Error thrown by yay")
			log.Fatal(yayError)
		}
	} else if args[0] == "update" {
		yayCommand := exec.Command( "sh", "-c", "yay -Su")
		yayCommand.Stdout = os.Stdout
		yayCommand.Stdin = os.Stdin
		yayCommand.Stderr = os.Stderr
		yayError := yayCommand.Run()
		if yayError != nil {
			fmt.Println("Error thrown by yay")
			log.Fatal(yayError)
		}
	} else if args[0] == "upgrade" {
		yayCommand := exec.Command( "sh", "-c", "yay -Syu")
		yayCommand.Stdout = os.Stdout
		yayCommand.Stdin = os.Stdin
		yayCommand.Stderr = os.Stderr
		yayError := yayCommand.Run()
		if yayError != nil {
			fmt.Println("Error thrown by yay")
			log.Fatal(yayError)
		}
	} else if args[0] == "list" {
		yayCommand := exec.Command( "yay", "-Q")
		yayCommand.Stdout = os.Stdout
		yayCommand.Stdin = os.Stdin
		yayCommand.Stderr = os.Stderr
		yayError := yayCommand.Run()
		if yayError != nil {
			fmt.Println("Error thrown by yay")
			log.Fatal(yayError)
		}
	} else if args[0] == "interactive" {
		cmdArr :=[]string{"yay", strings.Join(args[1:], " ")}
		cmdStr := strings.Join(cmdArr, " ")
		yayCommand := exec.Command("sh", "-c", cmdStr)
		yayCommand.Stdout = os.Stdout
		yayCommand.Stdin = os.Stdin
		yayCommand.Stderr = os.Stderr
		yayError := yayCommand.Run()
		if yayError != nil {
			fmt.Println("Error thrown by yay")
			log.Fatal(yayError)
		}
	} else {
		fmt.Println("Incorrect command provided!")
		printHelpScreen()
		os.Exit(1)
	}
}

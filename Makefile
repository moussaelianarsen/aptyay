SRCDIR ?= .

aptyay: aptyay.go
	go build aptyay.go

install: aptyay
	install -Dm755 $(SRCDIR)/aptyay $(DESTDIR)/usr/bin/aptyay
